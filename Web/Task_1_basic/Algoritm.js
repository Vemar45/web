function Torch(x_v, y_v,fun,wal) {
    this.weight=fun;
    this.x = x_v;
    this.y = y_v;
    this.h = 0;
    this.f = 0;
    this.g = 0;
    this.parent = null;
    this.wall=wal;
    this.close=false;
    this.visited = false;
  }
   
  function Graph(matr_walls,n) {
    this.arr = [];
    for (var i = 0; i < n;i++) {
      this.arr[i] = [];
    }
    for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++)
        this.arr[i][j] = new Torch(i, j,1, matr_walls[i][j]);
    }
  }
   
  function heuristic(start, end) {        //Манхетоновское расстояние
    var d1 = Math.abs(start.x - end.x);
    var d2 = Math.abs(start.y - end.y);
    return d1 + d2;
  }
   
  function return_arr_neib(graph, n, torch) {
    var arr = [];
    var x = torch.x;
    var y = torch.y;
    if (y - 1 >= 0)
      arr.push(graph.arr[x][y - 1]); //верхняя клетка
    if (y + 1 < n)
      arr.push(graph.arr[x][y + 1]);//нижняя
    if (x - 1 >= 0)
      arr.push(graph.arr[x-1][y]); //Левая клетка
    if (x + 1 < n)
      arr.push(graph.arr[x+1][y]); // Правая клетка
    return arr;
  }
   
  function path(torch) {
    var curr = torch;
    var path = [];
    while (curr.parent!=null) {
      path.unshift(curr);
      curr = curr.parent;
    }
    path.unshift(curr);
    return path;
  }
  function BinaryHeap(scoreFunction) {
    this.content = [];
    this.scoreFunction = scoreFunction; // Какой элемент будет для нас являться приоритетом
  }
  BinaryHeap.prototype.sinkDown = function (n) {
    var element = this.content[n];
   
    while (n > 0) {
   
      // Вычислить индекс родительского элемента и извлечь его.
      var parentN = ((n + 1) >> 1) - 1;
      var parent = this.content[parentN];
      // Менять местами элементы, если родитель больше.
      if (this.scoreFunction(element) < this.scoreFunction(parent)) {
        this.content[parentN] = element;
        this.content[n] = parent;
        // Обновить "n", чтобы продолжить работу на новой позиции.
        n = parentN;
      }
      // Нашел родителя, который меньше, не нужно брать дальше.
      else {
        break;
      }
    }
  };
   
  BinaryHeap.prototype.Push = function (element) {
     //Добавление элемента в конец массива
      this.content.push(element);
   
      // Просиваем
      this.sinkDown(this.content.length - 1);
  };
  BinaryHeap.prototype.Pop = function (element) {
     var result = this.content[0];
      // Получить элемент с конца массива
      var end = this.content.pop();
      if (this.content.length > 0) {
        this.content[0] = end;
        this.bubbleUp(0);
      }
      return result;
   
   
  };
  BinaryHeap.prototype.Remove = function (node) {
    var i = this.content.indexOf(node);

    var end = this.content.pop();
   
    if (i !== this.content.length - 1) {
      this.content[i] = end;
   
      if (this.scoreFunction(end) < this.scoreFunction(node)) {
        this.sinkDown(i);
      } else {
        this.bubbleUp(i);
      }
    }
  };
  BinaryHeap.prototype.Size= function () {
    return this.content.length;
  };
  BinaryHeap.prototype.rescoreElement = function (node) {
    this.sinkDown(this.content.indexOf(node));
  };
  BinaryHeap.prototype.bubbleUp = function (n) {
      // Смотрим на целевой элемент и его оценку.
      var length = this.content.length;
      var element = this.content[n];
      var elemScore = this.scoreFunction(element);
   
      while (true) {
        // Вычислите индексы дочерних элементов.
        var child2N = (n + 1) << 1;
        var child1N = child2N - 1;
        // Это используется для хранения новой позиции элемента, если таковая имеется.
        var swap = null;
        var child1Score;
        // Если первый дочерний элемент существует (находится внутри массива)
        if (child1N < length) {
          var child1 = this.content[child1N];
          child1Score = this.scoreFunction(child1);
   
          // Если счет меньше, чем у нашего элемента, нам нужно поменяться местами.
          if (child1Score < elemScore) {
            swap = child1N;
          }
        }
   
        // Сделайте то же самое для другого ребенка.
        if (child2N < length) {
          var child2 = this.content[child2N];
          var child2Score = this.scoreFunction(child2);
          if (child2Score < (swap === null ? elemScore : child1Score)) {
            swap = child2N;
          }
        }
   
        // Если элемент нужно переместить, замените его и продолжайте.
        if (swap !== null) {
          this.content[n] = this.content[swap];
          this.content[swap] = element;
          n = swap;
        }
        else {
          break;
        }
      }
  };
  function getHeap() {
    return new BinaryHeap(function(node) {
      return node.f;
    });
  }
   
   
   
   
  function A_star(graph,n, start, end) {
    var openHeap = getHeap();
    start.h = heuristic(start, end);
    openHeap.Push(start);
    var torch_now;
    var closest = start;
    var flag = false; // флаг нужен для того чтобы отображать предп. путь в усложненном варианте
   
   
    while (openHeap.Size() > 0) {
      torch_now = openHeap.Pop(); //Выбираем элемент с мин значением f;
      if (torch_now.x === end.x && torch_now.y === end.y)
        return path(torch_now);
      torch_now.close = true;
      var neib_arr = return_arr_neib(graph, n, torch_now);
      var neib_asize = neib_arr.length;
      for (var i = 0; i < neib_asize; i++) {
        var neibghor = neib_arr[i];
        if (neibghor.close || neibghor.wall === 1)
          continue;
        
        var g_weight = neibghor.weight + torch_now.g;
        var vis = neibghor.visited;
        //Находим оптимальный путь к точке
        if (!vis || g_weight < neibghor.g) {
          neibghor.visited = true;
          neibghor.parent = torch_now;
          neibghor.h = heuristic(neibghor, end);
          neibghor.g = g_weight;
          neibghor.f = neibghor.g + neibghor.h;
          if (flag) {
            if (neibghor.h < closest.h || (neibghor.h === closest.h && neibghor.g < closest.g)) {
              closest = neibghor;
            }
          }
          if (!vis) //если мы в ней не были то вставляем в очередь
            openHeap.Push(neibghor);
          else
            openHeap.rescoreElement(neibghor); //Изменяем значение точки, у который был пересчет функции 
        }      
      }
    }
    return [];
  }

  export {A_star,Graph};