let map = [];
let checked =[];
let x,y;



function Point(x_v, y_v) {
    this.x = x_v;
    this.y = y_v;
}


// с помощью алгоритма Прима
export let generat_maze = (size,Start,Finish) => {
    for(let i=0;i<size;i++){
        map[i] = [];
        checked[i]=[];
    }
    for(let i=0;i<size;i++){
        for(let j=0;j<size;j++){
            map[i][j] = 1;
            checked[i][j]=0;
        }
    }
    x = parseInt(Math.random() * parseInt(size/2))*2 + 1;
    y = parseInt(Math.random() *  parseInt(size/2))*2 + 1;
    map[x][y]= 0;

    let check=[];
    if(y-2>=0){
        check.push(new Point(x,y-2));
        checked[x][y-2]=1;
    }
    if(y+2<size){
        check.push(new Point(x,y+2));
        checked[x][y+2]=1;
    }
    if(x-2>=0){
        check.push(new Point(x-2,y));
        checked[x-2][y]=1;
    }
    if(x+2<size){
        check.push(new Point(x+2,y));
        checked[x+2][y]=1;
    }

    let number, direction, dir;
   
    while(check.length>0){
    number = parseInt(Math.random() * check.length);
    x = check[number].x;
    y =check[number].y;
    map[check[number].x][check[number].y] = 0;
    check.splice(number,1);
    direction = [1,2,3,4];
    while(direction.length>0){
        dir = parseInt(Math.random()*direction.length);
        switch(direction[dir]){
            case 1:{   
                if (y - 2 >= 0 && map[x][y - 2]==0) {
                    map[x][y - 1]=0;
                    direction =[];
                }else {
                    direction.splice(dir,1);
                }
                break;
            }
            case 2:{
                if (y + 2 < size && map[x][y + 2]==0) {
                    map[x][y + 1]=0;
                    direction =[];
                }else {
                    direction.splice(dir,1);
                }
                break;
            }
            case 3:{
                if (x - 2 >= 0 && map[x - 2][y]==0) {
                    map[x - 1][y]=0;
                    direction =[];
                }else {
                    direction.splice(dir,1);
                }
                break;
            }
            case 4:{
                if (x + 2 < size && map[x + 2][y]==0) {
                    map[x + 1][y]=0;
                    direction =[];
                } else {
                    direction.splice(dir,1);
                }
                break;
            }
        }

        if(y-2>=0 && map[x][y - 2]==1 && checked[x][y-2]==0){
            check.push(new Point(x,y-2));
            checked[x][y-2] = 1;
        }
        if(y+2<size && map[x][y + 2]==1&& checked[x][y+2]==0){
            check.push(new Point(x,y+2));
            checked[x][y+2] = 1;
        }
        if(x-2>=0 && map[x - 2][y]==1&& checked[x-2][y]==0){
            check.push(new Point(x-2,y));
            checked[x-2][y] = 1;
        }
        if(x+2<size && map[x + 2][y]==1&& checked[x+2][y]==0){
            check.push(new Point(x+2,y));
            checked[x+2][y] = 1;
        }
    }
    }
    direction = [1,2,3,4];
    dir = parseInt(Math.random()*direction.length);
    let arr=[];
    switch(direction[dir]){
        case 1:{   
            for(let i=0;i<size;i++){
                if(map[i][1]==0){
                    arr.push(i);
                }
            }
            x = arr[parseInt(Math.random()*arr.length)];
            map[x][0] = 2;
            Start.X = x;
            Start.Y = 0;
            arr = [];
            for(let i=0;i<size;i++){
                if(map[i][size-2]==0){
                    arr.push(i);
                }
            }
            x = arr[parseInt(Math.random()*arr.length)];
            map[x][size-1] = 3;
            Finish.X = x;
            Finish.Y = size-1;
            break;
        }
        case 2:{
            for(let i=0;i<size;i++){
                if(map[size-2][i]==0){
                    arr.push(i);
                }
            }
            y = arr[parseInt(Math.random()*arr.length)];
            map[size-1][y]=2;
            Start.X = size-1;
            Start.Y = y;
            arr = [];
            for(let i=0;i<size;i++){
                if(map[1][i]==0){
                    arr.push(i);
                }
            }
            y = arr[parseInt(Math.random()*arr.length)];
            map[0][y]=3;
            Finish.X = 0;
            Finish.Y = y;
            break;
        }
        case 3:{
            for(let i=0;i<size;i++){
                if(map[i][size-2]==0){
                    arr.push(i);
                }
            }
            x = arr[parseInt(Math.random()*arr.length)];
            map[x][size-1] = 2;
            Start.X = x;
            Start.Y = size-1;
            arr = [];
            for(let i=0;i<size;i++){
                if(map[i][1]==0){
                    arr.push(i);
                }
            }
            x = arr[parseInt(Math.random()*arr.length)];
            map[x][0] = 3;
            Finish.X = x;
            Finish.Y = 0;
            break;
        }
        case 4:{
            for(let i=0;i<size;i++){
                if(map[1][i]==0){
                    arr.push(i);
                }
            }
            y = arr[parseInt(Math.random()*arr.length)];
            map[0][y]=2;
            Start.X = 0;
            Start.Y = y;
            arr = [];
            for(let i=0;i<size;i++){
                if(map[size-2][i]==0){
                    arr.push(i);
                }
            }
            y = arr[parseInt(Math.random()*arr.length)];
            map[size-1][y]=3;
            Finish.X = size -1;
            Finish.Y = y;
            break;
        }
    }
    Finish.cells_left =0;
    Start.cells_left =0;
    return map;
};



