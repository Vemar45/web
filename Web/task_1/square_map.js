import {A_star,Graph,timer_generat} from './Algoritm.js'
import {generat_maze} from './generating_maze.js'
let canvas_map = document.getElementById('map'); // ссылка на canvas
let canvas_path = document.getElementById('map2');
let canvas_length = canvas_map.clientWidth; // ширина canvas
let label_with_count_of_cells = document.getElementById('cells');
let start_button = document.getElementById('button_start');
let finish_button = document.getElementById('button_finish');
let stop_button = document.getElementById('button_stop');
let clear_button = document.getElementById('button_clear');
let search_button = document.getElementById('button_searching');
let generate_maze = document.getElementById('generate_maze');
let stop = document.getElementById('stop');
let map_size; //Количество клеток
let cell_size; // ширина и высота клетки cell_size = canvas_length/map_size;
let mouse_value; // вид новой клетки (временная заплатка)
let matrix; // карта в виде матрицы 0 - заблокированная клетка; 1 - свободная клетка; 2 - старт; 3 - финиш;
let map_context, path_context;
let is_path_found = false;
let has_map_created = false;



/* виды клеток: название, ссылка на картинки этих клеток, резервное имя для картинки, к
оличество клеток данного типа(если значения нет=> ограничений нет, ограничения 
заданы при нажатия на кнопку создания новой матрицы), значение в матрице*/
let Stop = {
    name:'Stop',
    image_src:'images/Stop.jpg',
    image_alt:'Stop',
    cells_left: 0,
    value_in_matrix: 1
};

let Clear = {
    name:'Clear',
    image_src:'images/Clear.jpg',
    image_alt:'Clear',
    value_in_matrix: 0
};

let Start = {
    name:'Start',
    image_src:'images/Start.jpg',
    image_alt:'Start',
    cells_left: 0,
    X:-1,
    Y:-1,
    value_in_matrix: 2
};

let Finish = {
    name: 'Finish',
    image_src:'images/Finish.jpg',
    image_alt:'Finish',
    cells_left: 0,
    X:-1,
    Y:-1,
    value_in_matrix: 3
};



let stop_img = new Image();
stop_img.src = Stop.image_src;
stop_img.alt = Stop.image_alt;
let start_img = new Image();
start_img.src =Start.image_src;
start_img.alt = Start.image_alt;
let clear_img = new Image();
clear_img.src = Clear.image_src;
clear_img.alt = Clear.image_alt;
let finish_img = new Image();
finish_img.src = Finish.image_src;
finish_img.alt = Finish.image_alt;
 

let clearCanvas=(name_canvas)=>{
  switch(name_canvas){
      case 'canvas_path':{
          path_context.beginPath();
          path_context.clearRect(0, 0, canvas_path.width, canvas_path.width);
          path_context.stroke();
          clearTimeout(timer_generat);
          break;
        } 

        case 'canvas_map':{
            map_context.beginPath();
            map_context.clearRect(0, 0, canvas_map.width, canvas_map.width);
            map_context.stroke();
            break;
        }   
    }
}


let pressing_cell_type_selection_button = (button_name) =>{
    disable_button(button_name);
    enable_button(mouse_value,button_name);
    mouse_value = button_name;
};


// выбор клетки старт
start_button.addEventListener("click",function(e){
    pressing_cell_type_selection_button(Start.name);
});

// выбор клетки финиш
finish_button.addEventListener("click",function(e){
    pressing_cell_type_selection_button(Finish.name);
});

// выбор пустой клетки 
clear_button.addEventListener("click",function(e){
    pressing_cell_type_selection_button(Clear.name);
});

// выбор клетки стоп
stop_button.addEventListener("click",function(e){
    pressing_cell_type_selection_button(Stop.name);
});

stop.addEventListener("click",function(e){
    clearTimeout(timer_generat);
});



export let check_way = (way) =>{
    if(way.length==0){
        alert('Пути нет'); 
    } else {
        draw_way(way,"#00EEEE");   
    }   
};


let graph;

//начать поиск пути от начала до конца
search_button.addEventListener("click",function(e){
    console.log(Finish.cells_left);
    if(has_map_created){
        clearCanvas("canvas_path");
        if(Start.cells_left==0&&Finish.cells_left==0){        
            matrix[Start.X][Start.Y] = 0;         
            matrix[Finish.X][Finish.Y] = 0;          
            graph = new Graph(matrix,map_size);         
            A_star(graph,map_size,graph.arr[Start.X][Start.Y],graph.arr[Finish.X][Finish.Y]);   
            matrix[Start.X][Start.Y] = 2;         
            matrix[Finish.X][Finish.Y] = 3;
            is_path_found = true;     
        } else if(Start.cells_left!=0&&Finish.cells_left!=0){ 
            alert('Нет точки старта и конца. Нажмите на кнопку '+ start_button.textContent+ 
            ', чтобы расположить точку старта, кнопку ' + finish_button.textContent+", чтобы расположить точку финиша"); 
        } else if(Start.cells_left!=0){
            alert('Нет точки старта. Расположите её нажав на кнопку '+ start_button.textContent); 
        } else {
            alert('Нет точки конца. Расположите её нажав на кнопку ' + finish_button.textContent); 
        }
    } else {    
        alert('Карты еще нет. Создайте ее нажав на кнопку '+ generate_maze.textContent); 
    }
});


// кнопка (start|finish|clear|stop) графически становиться не выбраной = активной.
let enable_button = (old_button,new_button) =>{
    if(old_button == new_button){
        return;
    }
    switch(old_button){
        case Start.name: {
           start_button.style.background = "#FF69B4";  
           start_button.style.color = 'black';
           break;
        }
        case Finish.name: {
           finish_button.style.background='#FA807A';                 
           finish_button.style.color= 'black';                       
           break;
        }
        case Clear.name: {
           button_clear.style.background='#FF7F50';                 
           button_clear.style.color= 'black';                       
           break;
        }
        case Stop.name: {
           stop_button.style.background = '#EE82EE';                 
           stop_button.style.color = 'black';                          
           break;
        }
        default:{
            break;
        }
    }
}

// кнопка (start|finish|clear|stop) графически становиться  выбраной = неактивной. Нажимать можно, но ничего не будет происходить
let disable_button = (str) =>{
 switch(str){
     case Start.name: {
        start_button.style.background = "#D8BFD8";
        start_button.style.color = 'white';       
        break;
     }
     case Finish.name: {
        finish_button.style.background='#FFA07A';                 
	    finish_button.style.color= 'white';                       
        break;
     }
     case Clear.name: {
        button_clear.style.background='#FF6347';                 
        button_clear.style.color= 'white';                        
        break;
     }
     case Stop.name: {
        stop_button.style.background = '#D8BFD8';                 
        stop_button.style.color = 'white';                       
        break;
     }
     default:{
         break;
     }
 }
};








// если canvas поддерживается, то получаем область для рисование(getContext('2d'))
    if(canvas_map && canvas_map.getContext) {
        map_context = canvas_map.getContext('2d');   
        path_context = canvas_path.getContext('2d');
    }


    generate_maze.addEventListener("click",function(e){
        has_map_created = true;
        map_size = label_with_count_of_cells.value;
        if(map_size!=""){
        if(map_size<5) map_size =5;
        if(map_size >  29) map_size = 29;
        if(map_size%2==0){
            map_size++;
        }
        label_with_count_of_cells.value = map_size;
        map_size=map_size - 0 + 2;
        matrix = generat_maze(map_size,Start,Finish);
        cell_size = canvas_length/map_size; // ширина и высота клетки
        Stop.cells_left = map_size*map_size-2;
        clearCanvas('canvas_map');
        clearCanvas('canvas_path');
        draw_map();
        } else {
          alert("Введите размер карты");
        }
    });

    
    let draw_map = () => {
        for(let i=0;i<map_size;i++){            
            for(let j=0;j<map_size;j++){ 
                switch(matrix[i][j]){
                    case Stop.value_in_matrix:{
                        map_context.drawImage(stop_img,i*cell_size+1,j*cell_size+1,cell_size-1,cell_size-1);  
                        break;
                    }
                    case Start.value_in_matrix:{
                        map_context.drawImage(start_img,i*cell_size+1,j*cell_size+1,cell_size-1,cell_size-1);  
                        break;
                    }
                    case Finish.value_in_matrix:{
                        map_context.drawImage(finish_img,i*cell_size+1,j*cell_size+1,cell_size-1,cell_size-1);  
                        break;
                    }
                    case Clear.value_in_matrix:{
                        map_context.drawImage(clear_img,i*cell_size+1,j*cell_size+1,cell_size-1,cell_size-1);  
                        break;
                    }
                }
             }
        }
        map_context.stroke();
    };
   

    let get_img_src = () =>{
        switch(mouse_value){
            case Stop.name: return Stop.image_src;
            case Clear.name: return Clear.image_src;
            case Start.name: return Start.image_src;
            case Finish.name: return Finish.image_src;
            default: return Clear.image_src;
        }
    };

    let get_img_alt = () =>{
        switch(mouse_value){
            case Stop.name: return Stop.image_alt;
            case Clear.name: return Clear.image_alt;
            case Start.name: return Start.image_alt;
            case Finish.name: return Finish.image_alt;
            default: return Clear.image_alt;
        }
    };

    // слушатель нажатий на клетки в canvas. Выбирает нужную картинку, рисует и записывает новое значение в матрицу.
    canvas_path.addEventListener("click", function(e){  
        if(has_map_created){
        let img_cell_change = new Image();
        if(is_path_found=true){
            clearCanvas('canvas_path');
            is_path_found = false;
        }     
        img_cell_change.src = get_img_src();            
        img_cell_change.alt = get_img_alt(); 
        img_cell_change.onload = function() {
            if(setCell(Math.round(down_to_cell_sizeX(e.offsetX)/cell_size),Math.round(down_to_cell_sizeY(e.offsetY)/cell_size))){
                map_context.drawImage(img_cell_change,down_to_cell_sizeX(e.offsetX)+1,down_to_cell_sizeY(e.offsetY)+1,cell_size-1,cell_size-1); 
            }
        }
    }
    });  


    


    

    let setCell = (x,y) =>{
        if(x>=map_size||x<0||y>=map_size||y<0){
            return false;
        }
        switch(mouse_value){
            case Stop.name: 
            if(replacement_controller(x,y,Stop)){
                matrix[x][y]=Stop.value_in_matrix;
                return true;
            } else {
                return false;
            }
            case Clear.name:
            if(replacement_controller(x,y,Clear)){
                matrix[x][y]=Clear.value_in_matrix;
                return true;
                } else {
                    return false;
                }
            case Start.name: 
            if(replacement_controller(x,y,Start)) {
                matrix[x][y]=Start.value_in_matrix;
                Start.X = x;
                Start.Y = y;
                return true;
            } else {
                return false;
            }
            case Finish.name: 
            if(replacement_controller(x,y,Finish)) {
                matrix[x][y]=Finish.value_in_matrix;
                Finish.X = x;
                Finish.Y = y;
                return true;
            } else {
                return false;
            }
            default: 
            matrix[x][y]= Clear.value_in_matrix;
            return false;
        }
    };

    


    // при замене клетки 
    let replacement_controller = (x,y,newCell) =>{
        let is_correct = true;
        if(newCell!=Clear){
            if(newCell.cells_left>0 && matrix[x][y]!=newCell.value_in_matrix){
                newCell.cells_left--;
            } else {  
                is_correct = false;
            }
        } else if(matrix[x][y]==newCell.value_in_matrix){
            return false;
        }
        if(is_correct){
            if(newCell.value_in_matrix!=Start.value_in_matrix && matrix[x][y]==Start.value_in_matrix){        
                Start.cells_left++;
            }
            if(newCell.value_in_matrix!=Finish.value_in_matrix && matrix[x][y]==Finish.value_in_matrix){    
                Finish.cells_left++;   
            }     
            if(newCell.value_in_matrix!=Stop.value_in_matrix && matrix[x][y]==Stop.value_in_matrix){       
                Stop.cells_left++;      
            }
        } 
        return is_correct;
    };

    // возвращает начальную координату для клетки 
    let down_to_cell_sizeX = (x) => {
        return x-x%cell_size;
    };

    let down_to_cell_sizeY = (y) => {
        return y-y%cell_size;
    };


    

    export let draw_way = (arr,color="red") => {
    
        for(let i=0;i<arr.length-1;i++){
            draw_from_to(arr[i].x,arr[i].y,arr[i+1].x,arr[i+1].y,color);
        }
    };

    let draw_from_to = (x1,y1,x2,y2,color) => {
        x1 = x1*cell_size + cell_size/2;
        y1 = y1*cell_size + cell_size/2;
        x2 = x2*cell_size + cell_size/2;
        y2 = y2*cell_size + cell_size/2;
        path_context.beginPath();
        path_context.strokeStyle = color;
        path_context.moveTo(x1,y1);
        path_context.lineWidth = 5;
        path_context.lineCap = 'round';
        path_context.lineTo(x2,y2);
        path_context.stroke();
    };















    
  

    

    