let clusters;


let get_distance_between_clusters = (x1,y1,x2,y2) => {
    return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
};

let distance_between_clusters = (first,second,mode) => {
    switch(mode){
        case "Single_link_method":{
            return min_distance_between_clusters(first,second);
        }
        case "Full_link_method":{
            return max_distance_between_clusters(first,second);
        }
        case "Middle_link_method":{
            return average_distance_between_clusters(first,second);
        }
    }
    return null;
};

let average_distance_between_clusters = (first,second) => {
    let sum=0;

    for(let i=0;i<clusters[first].length;i++){
        for(let j=0;j<clusters[second].length;j++){
            sum+=get_distance_between_clusters(clusters[first][i].x,clusters[first][i].y,clusters[second][j].x,clusters[second][j].y);
        }
    }
    return sum/(clusters[first].length*clusters[second].length);
};

let min_distance_between_clusters = (first,second) => {
    let min=Infinity;
    let current;
    for(let i=0;i<clusters[first].length;i++){
        for(let j=0;j<clusters[second].length;j++){
            current=get_distance_between_clusters(clusters[first][i].x,clusters[first][i].y,clusters[second][j].x,clusters[second][j].y);
            if(current<min){
                min = current;
            }
        }
    }
    return min;
};

let max_distance_between_clusters = (first,second) => {
    let max=-1;
    let current;
    for(let i=0;i<clusters[first].length;i++){
        for(let j=0;j<clusters[second].length;j++){
            current=get_distance_between_clusters(clusters[first][i].x,clusters[first][i].y,clusters[second][j].x,clusters[second][j].y);
            if(current>max){
                max = current;
            }
        }
    }
    return max;
};

export let start_hierarchical_clustering = (points,count,mode) =>{
    clusters=[];
    let count_of_clusters = points.length;
    for(let i=0;i<points.length;i++){
        clusters[i]=[];
    }
    for(let i=0;i<points.length;i++){
        clusters[i][0]=points[i];
    }
    let min,current, first,second;
    while(count_of_clusters>count){
        min = Infinity;
        for(let i=0;i<count_of_clusters;i++){
            for(let j=0;j<count_of_clusters;j++){
                if(i!=j&&clusters[i].length!=0){
                    current = distance_between_clusters(i,j,mode);
                    if(current<min){
                        min = current;
                        first = i;
                        second = j;
                    }
                }
            }
        }
        for(let k=0;k<clusters[second].length;k++){
            clusters[first][clusters[first].length]=clusters[second][k];
        }
        clusters.splice(second,1);
        count_of_clusters--;
    }
    return(clusters);
};






