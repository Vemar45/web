import {start_hierarchical_clustering} from './Hierarchical_clustering.js'
import {work_with_elements} from './k_means.js';
import {k_median_method} from './k_medians.js';
let colors_canvas = document.getElementById("colors_and_methods"); 
let canvas_down = document.getElementById('cluster_field');
let canvas_up = document.getElementById('painting');
let method1_canvas = document.getElementById('method1');
let method2_canvas = document.getElementById('method2');
let method3_canvas = document.getElementById('method3');
let Agglomerative_clustering_mode = document.getElementById('mode');
let colors_context, down_context, up_context,method1_context,method2_context,method3_context;
if (canvas_down && canvas_down.getContext) {
  down_context = canvas_down.getContext('2d');
  up_context = canvas_up.getContext('2d');
  colors_context = colors_canvas.getContext('2d');
  method1_context = method1_canvas.getContext('2d');
  method2_context = method2_canvas.getContext('2d');
  method3_context = method3_canvas.getContext('2d');
}

let colors_arr=["#CD5C5C","#ADFF2F","#556B2F","#008B8B","#6A5ACD","#20B2AA","#FFC0CB",
"#FFD700","#0000FF","#7FFFD4","#FA8072","#F0E68C",
"#00FF00","#8B0000","#808000","#FF1493"
,"#00FA9A","#FFFF00","#FF00FF",
"#FFA07A","#00FFFF","#FF8C00","#006400","#EE82EE","#FFDAB9",
"#DB7093","#8A2BE2","#8B008B",
"#1E90FF","#32CD32","#FF0000","#000080","#C71585","#9932CC",
"#66CDAA","#4B0082","#98FB98","#800000","#FF00FF","#A9A9A9","#228B22","#8FBC8F","#4682B4","#FFE4E1","#2F4F4F"];

let create_colors_canvas = (methods,count_of_clusters) =>{
    let arr = [];
    let count=0;
    for(let i=0;i<methods.length;i++){
        arr[i]=[];
        for(let j=0;j<count_of_clusters[i];j++){
            arr[i].push(colors_arr[count]);
            count++;
        }
    }
    draw_colors_and_methods(arr,methods);
    return arr;
};


let draw_colors_and_methods = (colors_arr,methods_arr) => {

    colors_context.font = "20px Georgia";
    for(let i=0;i<methods_arr.length;i++){
        colors_context.fillStyle = "black";
        if(methods_arr[i]=="Agglomerative_clustering"){
          colors_context.fillText(methods_arr[i]+" ("+Agglomerative_clustering_mode.value +"):",10,(i+1)*colors_canvas.clientHeight/(methods_arr.length+1));
        } else {
          colors_context.fillText(methods_arr[i]+":",10,(i+1)*colors_canvas.clientHeight/(methods_arr.length+1));
        }
        for(let j=0;j<colors_arr[i].length;j++) {
            colors_context.fillStyle = colors_arr[i][j];
            colors_context.beginPath();
            colors_context.fillRect(10+j*(480/colors_arr[i].length),(i+1)*colors_canvas.clientHeight/(methods_arr.length+1)+6,(480/colors_arr[i].length),20);
            colors_context.stroke();
        }
    }
};


function Point(x_v, y_v) {
  this.x = x_v;
  this.y = y_v;
}

let button_start = document.getElementById('button_start_clustering');
let button_cl = document.getElementById('button_clear');
let arr_checkbox = document.getElementsByClassName('method');
let arr_numbers_types = document.getElementsByClassName('number_of_clusters');
let arr_methods=[];
let arr_value_clusters=[];
let count_of_methods;
let arr_methods_color;
let arr_methods_Points;
let has_clustered = false;


button_start.addEventListener("click", function (e) {
    if (has_clustered) {
        clearCanvas('colors_canvas');
        clearCanvas('canvas_up');
        clearCanvas('method1');
        clearCanvas('method2');
        clearCanvas('method3');
    }
    arr_methods=[];
    has_clustered = true;
    let counter_of_clusters = 0;
    count_of_methods = 0;
    for (var i = 0; i < arr_checkbox.length; i++) {
      if (arr_checkbox[i].checked) {
        arr_methods[count_of_methods] = arr_checkbox[i].value;
        arr_value_clusters[count_of_methods] = arr_numbers_types[i].value;
        if(arr_value_clusters[count_of_methods]<=0){
          alert("Количество кластеров должно быть больше 0");
          return;
        }
        counter_of_clusters += parseInt(arr_value_clusters[count_of_methods]);
        count_of_methods++;
      }
    }
    if(count_of_methods==0){
      alert("Выберите хотя бы один метод");
    } else if (counter_of_clusters > 45) {
      alert("Слишком много класстеров, максимальное количество = 45");
    } else {
    arr_methods_color = create_colors_canvas(arr_methods, arr_value_clusters);
    for(let i=0;i<count_of_methods;i++){
      printMethod(arr_methods[i],arr_value_clusters[i],i);
    }
  }
});
let arrPoint = [];
let pi = Math.PI;
let rad = 12;

button_cl.addEventListener("click", function (e){
  clearCanvas('colors_canvas');
  clearCanvas('canvas_up');
  clearCanvas('canvas_down');
  clearCanvas('method1');
  clearCanvas('method2');
  clearCanvas('method3');
  arrPoint =null;
  arrPoint = [];
});


let printMethod = (method_name,clusters,type) =>{
  switch(method_name){
    case "K_means":{
      arr_methods_Points = work_with_elements(arrPoint,clusters);
      break;
    }
    case "K_medians":{ 
      arr_methods_Points = k_median_method(arrPoint,clusters);
      break;
    }
    case "Agglomerative_clustering":{
      arr_methods_Points = start_hierarchical_clustering(arrPoint,clusters,Agglomerative_clustering_mode.value);
      break;
    }
    default:return;
  }
  switch(type){
    case 0:{
      method1_canvas.style.backgroundColor = "#ffffffce";
      method1_canvas.style.border="2px ridge rgb(201, 154, 52)";
      for(let l=0;l<arr_methods_Points.length;l++){
        for(let g=0;g<arr_methods_Points[l].length;g++){
          draw_on_small_canvas(arr_methods_Points[l][g],type,l,method1_context);
        }
      }
      break;
    }
    case 1:{
      method2_canvas.style.backgroundColor = "#ffffffce";
      method2_canvas.style.border="2px ridge rgb(201, 154, 52)";
      for(let l=0;l<arr_methods_Points.length;l++){
        for(let g=0;g<arr_methods_Points[l].length;g++){
          draw_on_small_canvas(arr_methods_Points[l][g],type,l,method2_context);
        }
      }
      break;
    }
    case 2:{
      method3_canvas.style.backgroundColor = "#ffffffce";
      method3_canvas.style.border="2px ridge rgb(201, 154, 52)";
      for(let l=0;l<arr_methods_Points.length;l++){
        for(let g=0;g<arr_methods_Points[l].length;g++){
          draw_on_small_canvas(arr_methods_Points[l][g],type,l,method3_context);
        }
      }
      break;
    }
  }
  for(let l=0;l<arr_methods_Points.length;l++){
    for(let g=0;g<arr_methods_Points[l].length;g++){
      torch_redraw(arr_methods_Points[l][g],type,l);
    }
  }
  arr_methods_Points = [];
};



let draw_on_small_canvas = (Point, method_count, color_count,context) =>{
  context.beginPath();
  context.fillStyle = arr_methods_color[method_count][color_count]; 
  context.arc(Point.x/2, Point.y/2, rad/1.75, 0, 2 * pi);
  context.fill();
  context.stroke();
};



   
canvas_up.addEventListener("mousedown", function (e) {
  if (has_clustered) {
    clearCanvas('colors_canvas');
    clearCanvas('canvas_up');
    clearCanvas('method1');
    clearCanvas('method2');
    clearCanvas('method3');
    has_clustered = false;
  }
  let x = e.offsetX;
  let y = e.offsetY;
  let point = new Point(x, y);
  arrPoint.push(point);
  down_context.beginPath();
  down_context.arc(x, y, rad, 0, 2 * pi);
  down_context.stroke();
});


function torch_redraw(Point, method_count, color_count) {
  switch (count_of_methods) {
    case 1:
      up_context.beginPath();
      up_context.fillStyle = arr_methods_color[method_count][color_count]; 
      up_context.arc(Point.x, Point.y, rad, 0, 2 * pi);
      up_context.fill();
      up_context.stroke();
      break;
    case 2:
      up_context.beginPath();
      up_context.fillStyle = arr_methods_color[method_count][color_count];
      up_context.arc(Point.x, Point.y, rad, 0 + pi *method_count, pi * (method_count + 1));
      up_context.fill();
      up_context.stroke();
      break;
    case 3:
      up_context.beginPath();
      up_context.fillStyle = arr_methods_color[method_count][color_count];
      up_context.arc(Point.x,Point.y, rad, 2 * Math.PI / 3 * method_count, 2 * Math.PI / 3 * (method_count + 1), false);
      up_context.lineTo(Point.x,Point.y);
      up_context.fill();
      up_context.stroke();
      break;
  }
}





let clearCanvas=(name_canvas)=>{
  switch(name_canvas){
      case 'canvas_up':{
        up_context.beginPath();
        up_context.clearRect(0, 0, canvas_up.width,canvas_up.height);
        up_context.stroke();
          break;
      }
      case 'canvas_down':{
        down_context.beginPath();
        down_context.clearRect(0, 0, canvas_down.width, canvas_down.height);
        down_context.stroke();
        break;
    }
    case 'colors_canvas': {
       colors_context.beginPath();
       colors_context.clearRect(0, 0, colors_canvas.width, colors_canvas.height);
       colors_context.stroke();
       break;
    }
    case 'method1': {
      method1_context.beginPath();
      method1_context.clearRect(0, 0, method1_canvas.width, method1_canvas.height);
      method1_context.stroke();
      method1_canvas.style.border = "0px";
      method1_canvas.style.backgroundColor = "";
      break;
    }
    case 'method2': {
      method2_context.beginPath();
      method2_context.clearRect(0, 0, method2_canvas.width, method2_canvas.height);
      method2_context.stroke();
      method2_canvas.style.border = "0px";
      method2_canvas.style.backgroundColor = "";
      break;
    }
    case 'method3': {
      method3_context.beginPath();
      method3_context.clearRect(0, 0, method3_canvas.width, method3_canvas.height);
      method3_context.stroke();
      method3_canvas.style.border = "0px";
      method3_canvas.style.backgroundColor = "";
      break;
    }
  }
}