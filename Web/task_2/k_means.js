let number_of_clusters;           // число кластеров
let etalon;      // эталонные элементы
let number_of_points;           // всего точек


function Point(x_v, y_v) {
  this.x = x_v;
  this.y = y_v;
}

let get_elements_random = () => {       

    let count = 0;
    let x,y;
    for (let i = 0; i < number_of_clusters; i++)                                // рандомные элементы в эталонные
    {
        x = Math.random() * 500;
        y = Math.random() * 500;
        etalon[count] =  new Point(x,y);
        count += 1;
    }
};

let find_cluster_for_element = element => {               // найти ближайший эталон и передать его
    let d = Math.sqrt(Math.pow(element.x - etalon[0].x, 2) + Math.pow(element.y - etalon[0].y, 2));
    let index_etalon = 0;
    
    for (let i = 1; i < number_of_clusters; i++)
    {
       if (Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2)) < d)
       {
            d = Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2));
            index_etalon = i;
       }
    }
    
    return index_etalon;
};

let etalon_for_element = element =>           // функция для перерасчета эталона для каждого элемента 
{   
    let d = Math.sqrt(Math.pow(element.x - etalon[0].x, 2) + Math.pow(element.y - etalon[0].y, 2));
    let index_etalon = 0;
    
    for (let i = 1; i < number_of_clusters; i++)
    {
       if (Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2)) < d)
       {
            d = Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2));
            index_etalon = i;
       }
    }
    
    // перерасчет эталона 
    etalon[index_etalon].x = (element.x + etalon[index_etalon].x)/2;    
    etalon[index_etalon].y = (element.y + etalon[index_etalon].y)/2;
};

let elements_to_clusters = (array, cluster2) =>{
    let cluster;
    for (let i = 0; i < number_of_points; i++)
    {
        cluster = find_cluster_for_element(array[i]);   // получили номер эталона = кластера (точнее, номер - 1), куда приписать элемент
        cluster2[cluster].push(array[i]);
    }
    return cluster2;
};


export let work_with_elements = (array,count) =>
{
    number_of_clusters = count;
    etalon = new Array (number_of_clusters); 
    get_elements_random();                   // получаем рандомно эталоны
    number_of_points = array.length;
    let cluster2 = new Array(number_of_clusters);
    for (let i = 0; i < number_of_clusters; i++)
    {
        cluster2[i] = new Array();
    }

    let cluster1 = [];                             // пока число, затем массив при присваивании ниже
    let stop =1000;
    while (JSON.stringify(cluster1)!=JSON.stringify(cluster2)&&stop>0)                      
    {
        stop--;
        for (let i = 0; i < cluster2.length; i++){
            cluster1[i] = [];
            for (let j = 0; j < cluster2[i].length; j++){
                cluster1[i][j] = cluster2[i][j];
            } 
        }                     
        for (let i = 0; i < number_of_clusters; i++)                 // создаем cluster2 заново, точнее очищаем его двумерные массивы
        {
            cluster2[i] = new Array();
        }
        
        for (let i = 0; i < number_of_points; i++)
        {   
            etalon_for_element(array[i]);     
        }
        cluster2 = elements_to_clusters(array, cluster2);  
    }
    return cluster2;
};

