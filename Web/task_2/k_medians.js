let number_of_clusters;           // число кластеров
let etalon;      // эталонные элементы
let number_of_points;           // всего точек
let cluster2;


function Point(x_v, y_v) {
  this.x = x_v;
  this.y = y_v;
}

let get_elements_of_etalon_random = () => {       
    let x,y;
    for (let i = 0; i < number_of_clusters; i++)                                // рандомные элементы в эталонные
    {
        x = Math.random() * 500;
        y = Math.random() * 500;
        etalon[i] =  new Point(x,y);
    }
};

let find_cluster_for_element = element => {               // найти ближайший эталон и передать его
    let d = Math.sqrt(Math.pow(element.x - etalon[0].x, 2) + Math.pow(element.y - etalon[0].y, 2));
    let index_etalon = 0;
    
    for (let i = 1; i < number_of_clusters; i++)
    {
       if (Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2)) < d)
       {
            d = Math.sqrt(Math.pow(element.x - etalon[i].x, 2) + Math.pow(element.y - etalon[i].y, 2));
            index_etalon = i;
       }
    }
    
    return index_etalon;
};

let elements_to_clusters = (array, cluster2) =>{
    let cluster;
    for (let i = 0; i < number_of_points; i++)
    {
        cluster = find_cluster_for_element(array[i]);   // получили номер эталона = кластера (точнее, номер - 1), куда приписать элемент
        cluster2[cluster].push(array[i]);
    }
    return cluster2;
};

let compare =(a, b) =>{
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  }

let create_new_etalon = () =>{
    let arr_x = [];
    let arr_y = [];
    for(let i=0;i<cluster2.length;i++){ 
        for(let j=0;j<cluster2[i].length;j++){
            arr_x[j]=cluster2[i][j].x;
            arr_y[j]=cluster2[i][j].y;
        }
        if(arr_x.length!=0){
        arr_x.sort(compare);
        arr_y.sort(compare);
        if(cluster2[i].length%2==0){
            if(Math.random()>0.5){
                etalon[i] = new Point(arr_x[cluster2[i].length/2],arr_y[cluster2[i].length/2]);
            } else {
                etalon[i] = new Point(arr_x[cluster2[i].length/2-1],arr_y[cluster2[i].length/2-1]);
            }
        } else {    
            etalon[i] = new Point(arr_x[parseInt(cluster2[i].length/2)],arr_y[parseInt(cluster2[i].length/2)]);
        }
        
        arr_x = [];
        arr_y = [];
    }
    }
}


export let k_median_method = (array,count) =>
{
    number_of_clusters = count;
    etalon = []; 
    get_elements_of_etalon_random();                   // получаем рандомно эталоны
    number_of_points = array.length;
    cluster2 = [];
    for (let i = 0; i < number_of_clusters; i++)
    {
        cluster2[i] = [];
    }

    let cluster1 = [];   
    let stop =1000;                        
    while (JSON.stringify(cluster1)!=JSON.stringify(cluster2)&&stop>0)                        // пока разные элементы в кластерах каждый раз
    {  
        stop--;
        for (let i = 0; i < cluster2.length; i++){
            cluster1[i] = [];
            for (let j = 0; j < cluster2[i].length; j++){
                cluster1[i][j] = cluster2[i][j];
            } 
        }                 
        for (let i = 0; i < number_of_clusters; i++)                 // создаем cluster2 заново, точнее очищаем его двумерные массивы
        {
            cluster2[i] = [];
        }
        cluster2 = elements_to_clusters(array, cluster2);   
        create_new_etalon();
    }
    return cluster2;
}


