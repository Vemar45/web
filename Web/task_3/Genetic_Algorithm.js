import {draw_way,clearCanvas,distance,generation} from "./canvas.js"
let matrix_distance, population,count_of_chromosomes, number_of_generations;
export let timer_generat;
let points;



export let start_genetic_algorithm = (input_points) =>{
    if(input_points.length<2){
        return;
    }
    points = input_points;
    matrix_distance =[];
    population = [];
    count_of_chromosomes;
    number_of_generations = 1;
    for(let i=0;i<points.length;i++){
        matrix_distance[i] = [];
    }

    for(let i=0;i<matrix_distance.length;i++){
        for(let j=0;j<matrix_distance.length;j++){
            if(i!=j){
                matrix_distance[i][j] = get_distance(points[i],points[j]);
            } else {
                matrix_distance[i][j] = Infinity;
            }
        }
    }
    

    count_of_chromosomes = points.length*50;

    for(let i=0;i<count_of_chromosomes;i++){
        population[i] = [];
    }

    for(let i=0;i<count_of_chromosomes;i++){
        for(let j=0;j<points.length;j++){
            population[i][(j+i)%points.length] = j;
        }
    }
    timer_generat = setInterval(generate_new_population,500,points);
};


let generate_new_population = () =>{
    population = mutation();
    population = selection();
    for(let i=0;i<population.length;i++){
        population[i].splice(points.length,population[i].length-points.length);
    }
    clearCanvas('canvas_path');
    draw_way(get_the_best_chromosome());
    generation.textContent = number_of_generations;
    number_of_generations++;
    if(number_of_generations>100){
        draw_way(get_the_best_chromosome());
        clearTimeout(timer_generat);
    }
};


let get_the_best_chromosome = () =>{
    let chromosome_quality=[];
    for(let i=0;i<population.length;i++){
        chromosome_quality[i] = fitness_function(population[i]);
    }
    let max = -1;
    let answer =[];
    let maxJ = -1;
    for(let j=0;j<population.length;j++){
        if(chromosome_quality[j]>max){
             max = chromosome_quality[j];
             maxJ =j;
        }
    }
    for(let j=0;j<points.length;j++){
        answer[j]=points[population[maxJ][j]];
    }  
    let sum=0;
    for(let i=0;i<(answer.length-1);i++){
        sum+=matrix_distance[population[maxJ][i]][population[maxJ][i+1]];
    }
    sum+=matrix_distance[population[maxJ][answer.length-1]][0];
    distance.textContent = sum.toFixed(5);
    return answer;
};

let get_distance = (point1,point2) =>{
    return Math.sqrt(Math.pow(point1.x-point2.x,2)+Math.pow(point1.y-point2.y,2));
};


let selection = () =>{
    let chromosome_quality=[];
    for(let i=0;i<population.length;i++){
        chromosome_quality[i] = fitness_function(population[i]);
    }
    let max = -1;
    let answer  = [];
    let maxJ = -1;
    for(let i=0;i<count_of_chromosomes;i++){
        answer[i]=[];
        for(let j=0;j<population.length;j++){
            if(chromosome_quality[j]>max){
                max = chromosome_quality[j];
                maxJ = j;
            }
        }
        for(let j=0;j<population.length;j++){
            answer[i][j]=population[maxJ][j];
        }
        max =-1;
        chromosome_quality[maxJ] = -1;
        
    }
    return answer;
};




let fitness_function = (dots) =>{
    let sum=0;
    for(let i=0;i<(dots.length-1);i++){
        sum+=matrix_distance[dots[i]][dots[i+1]];
    }
    sum+=matrix_distance[dots[dots.length-1]][0];
    return 1/sum;
};

let mutation = () =>{
    let answer =[];
    let count_of_mutation,copy,x,y;
    for(let i=0;i<count_of_chromosomes;i++){
        answer[i] = [];
        for(let j=0;j<points.length;j++){
            answer[i][j]=population[i][j];
        }
    }
    
    for(let i=0;i<count_of_chromosomes;i++){
        count_of_mutation = parseInt(Math.random()*(100))+1;
        while(count_of_mutation>0){
            do {
            x = parseInt(Math.random()*answer[i].length);
            y = parseInt(Math.random()*answer[i].length);
            } while(x==y);
            copy = answer[i][x];
            answer[i][x] = answer[i][y];
            answer[i][y] = copy;
            count_of_mutation--;
        }
    }
    
    for(let i=count_of_chromosomes;i<(count_of_chromosomes*2);i++){
        answer[i] = [];
        for(let j=0;j<points.length;j++){
            answer[i][j]=population[i-count_of_chromosomes][j];
        }
    }
    return answer;
};