import {start_genetic_algorithm,timer_generat} from './Genetic_Algorithm.js'
let canvas_painting = document.getElementById('painting');
let canvas_path = document.getElementById('path');
let search_button = document.getElementById('start_genetic_algorithm');
let clear_button = document.getElementById('button_clear');
let stop_button = document.getElementById('button_stop');
export let distance = document.getElementById('distance');
export let generation = document.getElementById('generation_num');
let path_context, arr_of_points,painting_context;
let has_path = false
arr_of_points = [];

let num_color = 0;
let colors_arr=["#CD5C5C","#ADFF2F","#556B2F","#FFC0CB",
"#FFD700","#0000FF","#7FFFD4","#FA8072","#F0E68C",
"#00FF00","#8B0000","#808000","#FF1493"
,"#00FA9A","#FFFF00","#FF00FF",
"#FFA07A","#00FFFF","#FF8C00","#006400","#EE82EE","#FFDAB9",
"#DB7093","#8A2BE2","#8B008B",
"#32CD32","#FF0000","#000080","#C71585","#9932CC",
"#66CDAA","#4B0082","#98FB98","#800000","#FF00FF","#A9A9A9","#228B22","#8FBC8F","#FFE4E1","#2F4F4F"];

function Point(x_v, y_v) {
    this.x = x_v;
    this.y = y_v;
}

if(canvas_path && canvas_path.getContext) {   
   path_context = canvas_path.getContext('2d'); 
   painting_context = canvas_painting.getContext('2d');
}



search_button.addEventListener("click",function(e){
    if(has_path){
        clearCanvas("canvas_path");
        clearTimeout(timer_generat);
    }
    start_genetic_algorithm(arr_of_points);
    has_path = true;
});


clear_button.addEventListener("click",function(e){
    clearCanvas("canvas_path");
    clearCanvas("canvas_painting");
    clearTimeout(timer_generat);
    arr_of_points =null;
    arr_of_points = [];
    distance.textContent = 0;
    generation.textContent =1;
});

stop_button.addEventListener("click",function(e){
    clearTimeout(timer_generat);
});



let rad = 10;
let pi = Math.PI;

let x,y,point;
canvas_path.addEventListener('mousedown', function(e){  
    if (has_path) {
        clearCanvas('canvas_path');
        clearTimeout(timer_generat);
        has_path = false;
      }
      x = e.offsetX;
      y = e.offsetY;
      point = new Point(x, y);
      arr_of_points.push(point);
      painting_context.strokeStyle = "black";
      painting_context.lineWidth = 1;
      painting_context.beginPath();
      painting_context.arc(x, y, rad, 0, 2 * pi);
      painting_context.stroke();
});


export let draw_way = (arr) => {
    if(num_color>=colors_arr.length){
        num_color = 0;
    }
    path_context.strokeStyle = colors_arr[num_color];
    num_color++;
    if(arr!=undefined&&arr.length>0){
    for(let i=0;i<arr.length-1;i++){
        draw_from_to(arr[i],arr[i+1]);
    }
    draw_from_to(arr[arr.length-1],arr[0]);
}
};

let draw_from_to = (point1,point2) => {
    path_context.beginPath();
    path_context.moveTo(point1.x,point1.y);
    path_context.lineWidth = 4;
    path_context.lineCap = 'round';
    path_context.lineTo(point2.x,point2.y);
    path_context.stroke();
};



export let clearCanvas=(name_canvas)=>{
    switch(name_canvas){
        case 'canvas_painting':{
            painting_context.beginPath();
            painting_context.clearRect(0, 0, canvas_painting.width,canvas_painting.height);
            painting_context.stroke();
            break;
        }
        case 'canvas_path':{
            path_context.beginPath();
            path_context.clearRect(0, 0, canvas_path.width, canvas_path.height);
            path_context.stroke();
            break;
      }
    }
}







