let canvas = document.getElementById('point_field');
let canvas_path = document.getElementById('draw-line');
let button_clear = document.getElementsByClassName("buttons reset")[0];
let start_button = document.getElementsByClassName("buttons start")[0];
let path_length_text = document.getElementById("path_length");
let canvas_context, path_context;
var arr_of_points = [];
let has_path = false;
let rad = 10;
let pi = Math.PI;
let flag_on = false;
let flag_spawn;
let timer_wait;



function Point(x_v, y_v) {
    this.x = x_v;
    this.y = y_v;
}

function check_torch_spawn(x_center,y_center){
  if (arr_of_points.length!=0){
    for(var i=0;i<arr_of_points.length;i++){
      if(((arr_of_points[i].x-2*rad<=x_center) &&(arr_of_points[i].x+2*rad>=x_center))&&((arr_of_points[i].y-2*rad<=y_center) &&(arr_of_points[i].y+2*rad>=y_center)))
        return false;
    } 
  }
  return true;
}





if(canvas && canvas.getContext) {
   canvas_context = canvas.getContext('2d');   
   path_context = canvas_path.getContext('2d');
}
function draw_line_to_torches(point_first, point_second){
    path_context.beginPath();
    path_context.strokeStyle = "red";
    path_context.moveTo(point_first.x, point_first.y);
    path_context.lineWidth = 2;
    path_context.lineCap = 'round';
    path_context.lineTo(point_second.x, point_second.y);
    path_context.stroke();
};


function draw_path(short_path, arrpoint) {
    for (var i = 0; i < short_path.path.length; i++) {
        draw_line_to_torches(arrpoint[short_path.path[i].first], arrpoint[short_path.path[i].second]);
    }
};



start_button.addEventListener("click", function (e) {
    if (!flag_on) {
        path_length_text.textContent ="вычисляем...";
        setTimeout(start,0);
    }

});




let start =() =>{
 let n_points = arr_of_points.length;
        if (n_points > 2) {
            let arrdistance = [];
            for (var i = 0; i < n_points; i++)
                arrdistance[i] = [];
            for (var i = 0; i < n_points; i++)
                for (var j = 0; j < n_points; j++)
                    arrdistance[i][j] = 0;
            
            for (var i = 0; i < n_points; i++)
                for (var j = 0; j < n_points; j++)
                    if (i != j)
                        arrdistance[i][j] = get_dist(arr_of_points[i],arr_of_points[j]);
            var min_path = start_mur_algorithm(arrdistance);
            path_length_text.textContent = min_path.len.toFixed(3);
            draw_path(min_path,arr_of_points);
            flag_on = true;
        }
        else if(n_points===2){
          draw_line_to_torches(arr_of_points[0],arr_of_points[1]);
          path_length_text.textContent = get_dist(arr_of_points[0],arr_of_points[1]).toFixed(3);
        } else {
          path_length_text.textContent = 0;
    }
};


let get_dist = (point1,point2) => {
  return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2))
};





button_clear.addEventListener('mousedown', function(e){   
    clearCanvas("canvas");  
    clearCanvas("canvas_path");  
    path_length_text.textContent = ""; 
    arr_of_points =[];
});


let x,y,point;

canvas_path.addEventListener('mousedown', function (e) {
    if (flag_on)
      return;
    if (has_path) {
        clearCanvas('canvas_path');
        has_path = false;
      }
      x = e.offsetX;
      y = e.offsetY;
      flag_spawn=check_torch_spawn(x,y);
      if(flag_spawn){
        point = new Point(x, y);
        arr_of_points.push(point);
        canvas_context.beginPath();
        canvas_context.arc(x, y, rad, 0, 2 * pi);
        canvas_context.stroke();
      }
});


let draw_way = (arr) => {

    for(let i=0;i<arr.length-1;i++){
        draw_from_to(arr[i],arr[i+1]);
     }
};

let draw_from_to = (point1,point2) => {
    path_context.beginPath();
    path_context.strokeStyle = "red";
    path_context.moveTo(point1.x,point1.y);
    path_context.lineWidth = 5;
    path_context.lineCap = 'round';
    path_context.lineTo(point2.x,point2.y);
    path_context.stroke();
};

let clearCanvas = (name_canvas) => {
    flag_on = false;
    switch(name_canvas){
        case 'canvas':{
            canvas_context.beginPath();
            canvas_context.clearRect(0, 0, canvas.width,canvas.height);
            canvas_context.stroke();
            break;
        }
        case 'canvas_path':{
            path_context.beginPath();
            path_context.clearRect(0, 0, canvas_path.width,canvas_path.height);
            path_context.stroke();
            break;
        }
        case 'Text': {
            canvas_text_cont.beginPath();
            canvas_text_cont.clearRect(0, 0, canvas_text.width,canvas_text.height);
            canvas_text_cont.stroke();
            break;
        }    
    }
};










//Муравьиный алгоритм


function P_part (index,start,end)
{
  this.left=start+0.0;
  this.right=end+0.0;
  this.id=index;
}
 
function save_two_points(one, two) 
{
  this.first = one;
  this.second = two;
}
 
function save_paths_l(paths,len_path) 
{
  this.path = paths;
  this.len = len_path;
}
 
 
function choice(p_arrows)
 {
  let left = 0.0;
  let right=0.0;
  arr_p_parts = [];
  for (var i = 0; i < p_arrows.length; i++) {
    right =right+ p_arrows[i];
    arr_p_parts[i]=new P_part(i, left, right);
    left = right + 0.0;
  }
  let point = Math.random();
  for (var i = 0; i < arr_p_parts.length; i++)
    if (point > arr_p_parts[i].left && point < arr_p_parts[i].right)
      return arr_p_parts[i].id;
  
}
 
function range_to(lengthing)
{
    let array = new Array(lengthing);
    for (let i = 0; i < lengthing; i++)
    {
        array[i] = i;
    }
    return array;
}
 
function getstartphen(n) 
{
  let array_1 = new Array(n);
  for (let i = 0; i < n; i++)
    array_1[i] = [];
 
  let chance = 1.0 / n;
  for (let i = 0; i < n; i++)
    for (let j = 0; j < n; j++)
      array_1[i][j] = chance;
  return array_1;
};
 
function powarr(arr, stepen)
{
  for (var i = 0; i < arr.length; i++)
    arr[i] = Math.pow(arr[i], stepen);
  return arr;
};
 
function getdist(distance, stepen) 
{
  arr = [];
  for (var i = 0; i < distance.length; i++)
    arr[i] = 1.0 / distance[i];
  powarr(arr, stepen);
  return arr;
};
 
function mult_each_matr_el(arr1, arr2) {
  arr = [];
  for (var i = 0; i < arr1.length; i++)
    arr[i] = arr1[i] * arr2[i];
  return arr;
};
 
function Sum(arr) 
{
  let sum = 0.0;
  for (var i = 0; i < arr.length; i++)
    sum += arr[i];
  return sum;
};
 
function getrows(arrrow)
{
  arr_norm_rows = [];
  let sum = Sum(arrrow);
  for (var i = 0; i < arrrow.length; i++)
    arr_norm_rows[i] = arrrow[i] / sum;
  return arr_norm_rows;
};
 
function get_min_path(paths)  
{
  let min = 10000000.0;
  let min_path = new save_paths_l;
  for (let i = 0; i < paths.length; i++)
  {
    if (min > paths[i].len) 
    {
      min_path = paths[i];
      min = min_path.len;
    }
  }
  return min_path;
}
 
 
class Antcolony
{
    constructor(distances, n_ants, n_best, n_iterations, decay, alpha, beta)
    {
        let i = 0;
        let j = 0;
        while (i < distances.length)
        {
            while (j < distances.length)   // цикл не прервется в некоторых моментах
            {
                if (distances[i][j] == 0)
                {
                    distances[i][j] = Infinity;
                    i += 1;
                    j += 1;
                }
                else{
                  continue;
                }
            }
        }
        this.distances  = distances;               // матрица растояний
        this.n = distances.length;
        this.pheromone = getstartphen(this.n);     // данные о феромонах
        this.all_towns = range_to(distances.length);    //функция range_to в вершине файла   // список городов
        this.start_town = 0;
        this.n_ants = n_ants;                                                               // колличество муравьев
        this.n_best = n_best;                                                               // колличество элитных муравьев
        this.n_iterations = n_iterations;                                                   // колличество итераций
        this.decay = decay;                                                                 // испарения феромона
        this.alpha = alpha;
        this.beta = beta;            
    }
 
    pick_move(pheromone, dist, visited)
    {
      let new_pheromone = pheromone.slice();
      for (let i = 0; i < visited.length; i++)
        new_pheromone[visited[i]]=0;
      let left = powarr(new_pheromone, this.alpha);      // возведение в степень каждого феромона
      let right = getdist(dist, this.beta);              // Проверка здесь должна закончится
      let row = mult_each_matr_el(left, right);
      let norm_row = getrows(row);
      let move = choice(norm_row); 
      return move;
    }
 
    spread_pheronome(all_paths, n_best, shortest_path = shortest_path) 
    { 
        let move;
        let sorted_paths = all_paths.slice();
        sorted_paths.sort((a, b) => a.len - b.len);
        let path_1;
        let dist;
        for (let j = 0; j < n_best; j++)
        {
            path_1 = sorted_paths[j].path;
            dist = sorted_paths[j].len;
            for (let i = 0; i < path_1.length; i++)
            {
                move = path_1[i];
                this.pheromone[move.first][move.second] += 1.0 / this.distances[move.first][move.second];
                this.pheromone[move.second][move.first] += 1.0 / this.distances[move.second][move.first];
            }
        }
    }
 
    gen_path_dist(path)   
    {
        let total_dist = 0;
        let ele;
        for (let i = 0; i < path.length; i++)
        {
            ele = path[i];
            total_dist += this.distances[ele.first][ele.second];
      }
      
        return total_dist;
    }
 
    gen_path(start)    
    {
        let path_3 = [];
        let visited_3 = [];
        visited_3.push(start);
 
        let prev = start;
        let move;
        let save_e;
        for (let i = 0; i < this.distances.length - 1; i++)
        {
            move = this.pick_move(this.pheromone[prev], this.distances[prev], visited_3);
            save_e = new save_two_points(prev, move);
            path_3.push(save_e);
            prev = move;
            visited_3.push(move);
      }
        let save_e_1 = new save_two_points(prev, start);
        path_3.push(save_e_1);                                     //идем туда, откуда начали
        return path_3;
    } 
 
    gen_all_paths()  
    {
        let all_paths_2 = [];
        let path_2;
        let path_save_2;
        let start_point;
        for (let i = 0; i < this.n_ants; i++)
        {
          start_point = this.start_town;
          path_2 = this.gen_path(start_point);
          path_save_2 = new save_paths_l(path_2, this.gen_path_dist(path_2));
          all_paths_2.push(path_save_2);
          this.start_town++;
          if (this.start_town === this.n)
            this.start_town = 0;
      }
        return all_paths_2;
    }
 
    run()
    {
        let all_time_shortest_path = new save_paths_l("err", 10000000);
        let all_path, shortest_path = null; 
        for (let i = 0; i < this.n_iterations; i++)
        {
          all_path = this.gen_all_paths();
          this.spread_pheronome(all_path, this.n_best, shortest_path);
          shortest_path = get_min_path(all_path);  
            if (shortest_path.len < all_time_shortest_path.len)
            {
                all_time_shortest_path = shortest_path;
                i = 0;
            }    
            this.pheromone * this.decay;
        }
            
        return all_time_shortest_path;
    }
 
}

function start_mur_algorithm(matrix_distance) {
    let alpha = 1;
    let beta = 1;
    let arrdist = matrix_distance.slice();
    let ant_colony = new Antcolony(arrdist, arrdist.length * 2, 5, arrdist.length * 4, 0.95, alpha, beta);
    let shortest_path = ant_colony.run();
    return shortest_path;
}