import {start_analysis} from "./neuron_net.js";
let canvas = document.getElementById("field");
let button_Clear = document.getElementById("Clear");
export let label_answer = document.getElementById("answer");
let scan_button = document.getElementById("Scan");
let color = document.getElementsByName("color");
let canvas_context;
let pixels = [];
let canvas_size = 50;
let cell_size = canvas.width/canvas_size;
let definition_started = false;
for(let i=0;i<canvas_size*canvas_size;i++){
    pixels[i]=0;
}
if(canvas && canvas.getContext){
    canvas_context = canvas.getContext("2d");
    canvas_context.beginPath();
    canvas_context.fillStyle = "white";
    canvas_context.fillRect(0, 0, canvas.width, canvas.width);
    canvas_context.stroke();
}







let get_average_pixel_value = (arr) =>{
    let size = arr.length/4;
    let sum=0;
    for(let i =0;i<arr.length;i+=4){
            if(arr[i]==0){
                sum++;
            }
    }
    if(sum/size>0.49){
        return 1;
    } else {
        return 0;
    }
};



scan_button.addEventListener("click",function(e){
    for(let i=0;i<canvas_size;i++){
        for(let j=0;j<canvas_size;j++){
            pixels[j*canvas_size+i] = get_average_pixel_value(canvas_context.getImageData(i*cell_size, j*cell_size, cell_size, cell_size).data);
        }
    }
    start_analysis(pixels);
});





button_Clear.addEventListener("click",function(e){
    canvas_context.beginPath();
    canvas_context.fillStyle = "white";
    canvas_context.fillRect(0, 0, canvas.width, canvas.width);
    canvas_context.stroke();
    for(let i=0;i<canvas_size*canvas_size;i++){
        pixels[i]=0;
    }
    label_answer.textContent="";
});



let draw = false;
canvas.addEventListener('mousedown', function(e){
    if(definition_started){
        label_answer.textContent=""; 
        definition_started = false;
    } 
    draw = true;
    if(color[0].checked){
        canvas_context.strokeStyle = color[0].value;
    } else {
        canvas_context.strokeStyle = color[1].value;
    }
    canvas_context.lineCap = 'round';
    canvas_context.lineWidth = 12;
    canvas_context.beginPath();
    canvas_context.moveTo(e.offsetX,e.offsetY);
});  

canvas.addEventListener("mousemove", function(e){
                 
    if(draw){
        canvas_context.lineTo(e.offsetX,e.offsetY);
        canvas_context.stroke();
    }
});
canvas.addEventListener("mouseup", function(e){
    if(draw) {
    canvas_context.lineTo(e.offsetX,e.offsetY);
    canvas_context.stroke();
    canvas_context.closePath();
    draw = false;
    }
});


canvas.addEventListener("mouseout", function(e){
    if(draw){
        draw = false;
    }
});

  

  














