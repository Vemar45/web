const NUMBER_OF_LAYERS = 3;
import {get_weights} from './link_weights2.js';
import {label_answer} from './canvas.js';
export let matrix_weights_of_layers = [];
let delta_matrix_weights_of_layers = [];
let size_of_layers = [2500,125,10];
let layer_of_neurons_output = [];
let delta_layer_of_neurons = [];
for(let i=0;i<NUMBER_OF_LAYERS-1;i++){
    matrix_weights_of_layers[i]=[];
    delta_matrix_weights_of_layers[i]=[];
    delta_layer_of_neurons[i]=[]; 
}

get_weights();


for(let i=0;i<NUMBER_OF_LAYERS;i++){
    layer_of_neurons_output[i]=[];
}



let get_new_delta = (matrix) =>{
    let answer = [];
    for(let i=0;i<matrix.length;i++){
        answer[i]=[];
        for(let j=0;j<matrix[i].length;j++){
            answer[i][j]=0;
        }
    }
    return answer;
};

  

for(let i=0;i<NUMBER_OF_LAYERS-1;i++){
    delta_matrix_weights_of_layers[i] = get_new_delta(matrix_weights_of_layers[i]);
}

export let start_analysis = (arr) =>{
    for(let i=0;i<size_of_layers[0];i++){
        layer_of_neurons_output[0][i] = arr[i];
    }
    for(let i=1;i<NUMBER_OF_LAYERS;i++){
        layer_of_neurons_output[i] = matrix_multiplication(layer_of_neurons_output[i-1],matrix_weights_of_layers[i-1],size_of_layers[i]); 
    }
    let index = get_index_of_max(layer_of_neurons_output[NUMBER_OF_LAYERS-1]);
    label_answer.textContent = index;
};





let sigm = (x) => {
    return 1/(1+Math.exp(-x));
};


let matrix_multiplication = (matrix_neurons,matrix_weights,size) =>{
    let answer = [];
    let sum;
    for(let i=0;i<size;i++){
        sum=0;
        for(let j=0;j<matrix_neurons.length;j++){
            sum+=matrix_neurons[j]*matrix_weights[j][i];
        }
        answer[i]=sigm(sum);
    }
    return answer;
};





let get_index_of_max = (arr)=>{
    let max = -Infinity;
    let maxI=-1;
    for(let i=0;i<arr.length;i++){
        if(arr[i]>max){
            max = arr[i];
            maxI = i
        }
    }
    return maxI;
};










